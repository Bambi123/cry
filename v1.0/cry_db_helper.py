import sqlite3
from datetime import datetime

# Bulk Function to Update the Database structure
def cry_db_update_database(db, symbol, price_list):

    # Move through all prices and add them into the database
    for p in price_list:
        # generate query
        val = float(p)
        q = f'''INSERT INTO {symbol} (price) VALUES ({val})'''
        # Try and insert into database
        try:
            cur = db.cursor()
            cur.execute(q)
            cur.close()
        except Exception as e:
            print(f"Error: Could not update {symbol} | {p} into database: {e}")

    db.commit()


# Generate Symbol Function in SQL Database
def cry_db_create_symbol_table(db, table_name):

    # Query to generate synbol tavle
    q = f'''CREATE TABLE {table_name} (price float NOT NULL)'''

    print(q)

    # Attempt to ceate the table
    try:
        cur = db.cursor()
        cur.execute(q)
        cur.close()
        db.commit()
        return None
    except Exception as e:
        print(f"Error Creating Symbol Table {table_name}: {e}")

    print("...removing table and retrying")
    q_remove = f'''DROP TABLE {table_name}'''

    # Attempt to remove table and add it back
    try:
        cur = db.cursor()
        cur.execute(q_remove)
        cur.execute(q)
        db.commit()
    except Exception as e:
        print(f"Error: Could not remove and create the symbol table {table_name}: {e}")
        return None

# Connect to database
def cry_db_connect(path):

    try:
        return sqlite3.connect(path)
    except Exception as e:
        print(f"Error: Could not open a connection")
        return None

# Create initial database
def cry_db_create_database(path, wallet_assets):

    print("")
    print("---------------------------")
    print("CREATING Database(s)")
    print("---------------------------")

    # Get database connection
    db = cry_db_connect(path)
    if not db: return None

    # Generate SQL tables for all assets
    for w in wallet_assets: cry_db_create_symbol_table(db, w)

    # Return connection to the database
    return db

# Extract db's first entry from given table name
def cry_db_get_first(db, table_name):

    q = f'''select * from {table_name} LIMIT 1'''

    try:
        cur = db.cursor()
        cur.execute(q)
        data = cur.fetchall()[0][0]
        cur.close()
        return data
    except Exception as e:
        print(f"Error: Could not extract first element from {table_name}: {e}")

    return None

# Extract db's first entry from given table name
def cry_db_get_last(db, table_name):

    q = f'''select * from from {table_name} DESC LIMIT 1'''

    try:
        cur = db.cursor()
        cur.execute(q)
        data = cur.fetchall()[0][0]
        cur.close()
        return data
    except Exception as e:
        print(f"Error: Could not extract first element from {table_name}: {e}")

    return None


    return None


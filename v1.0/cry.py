# Binance API Python Library
from binance.client import Client

# User Configuration Library
try:
    import cry_config
except Exception as e:
    print("Error: You don't have all the configuration files to run this program!")
    print("This is likely because you downloaded from git and you will need a security key to access my wallet")
    print("I cannot give this to you (I like not being hacked).  Instead you can generate you own api keys")

# Cry helper Functions
from cry_helper import cry_client_wallet_assets, cry_start_output, cry_finish_output, cry_get_time_now
from cry_helper import cry_halt_program, cry_refresh_data

# Cry Analysis helper functions
from cry_analysis_helper import cry_analysis

# Global Variables
RUN_LOOP = 1
END_LOOP = 0

def collect_data(client):

    # Get Client Wallet Crypto Prices
    wallet_assets = cry_client_wallet_assets(client, "USDT")

    # Set up database
    db = cry_db_create_database(cry_db_config.database_path, wallet_assets)

    while RUN_LOOP:
        # Get Client Wallet Crypto Prices
        wallet_asset_prices = cry_client_wallet_prices(client, "USDT")
        # Insert Prices into a database
        cry_db_update(db, wallet_asset_prices)
        # Sleep for a small time
        print("\n...sleeping")
        halt_program(5)

# Starting Output for beginning of script
cry_start_output()

# Extract Client Information from Binance API
client = Client(cry_config.api_key, cry_config.api_security)

# Get the start time of the program
start_time = cry_get_time_now()

# Halt program for a moment before starting
print("\n...REFRESHING starting in")
cry_halt_program(5)

# Refresh historical data and update database
cry_refresh_data(client)

# Analyse historical data
#cry_analysis(client)

# Finishing output
cry_finish_output()

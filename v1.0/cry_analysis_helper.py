# HELPER file for analysing historycal cryptocurrency data

from cry_helper import cry_halt_program, cry_measure_date_string_DAY, cry_get_time_now, cry_retrieve_symbol_data
from cry_db_helper import cry_db_create_symbol_table, cry_db_connect, cry_db_update_database, cry_db_get_first
import cry_db_config

# List of Techniques we can Implement
# - BTC watcher -> analyse 24hr change and trade other assets accordingly
# - Pattern Watcher -> Trading patterns emerging in markets
# - 

CONTINUE_LOOP = 1
END_LOOP = 0

# Help output from command central analysis
def cry_analysis_command_help():

    print("")
    print("Please insert command from following selection of techniques")
    print(" @ BTC Watcher Analysis: b")
    print(" @ Pattern Watcher Analysis: p")
    print(" @ Exit: e")

# Trims data down to 4x24 data points: 96 data points
def cry_analysis_trim_data_DAY(data_set):
    if len(data_set) <= 96: return data_set

    trimmed_data_set = []
    for i in range(0, 96):
        trimmed_data_set.append(data_set[i])
    return trimmed_data_set

def cry_analysis_BTC_watcher_verdict(db):

    open_day_price = cry_db_get_first(db, "BTCUSDT")
    close_day_price = cry_db_get_last(db, "BTCUSDT")

    print(open_day_price)
    print(close_day_price)



    return None

def cry_analysis_command_BTC_watcher(client):

    # Define the targetted asset for BTC Watching Trading
    target_asset = "MATICUSDT"

    # Output print for starting analysis
    print("")
    print("---------------------------")
    print(f"ANALYSIS: BTCUSDT Watcher vs {target_asset}")
    print("---------------------------")
    print("")
    # Half program for a moment before stating
    print("...analysis starting")
    cry_halt_program(5)
    # Get link ot database
    db = cry_db_connect(cry_db_config.database_path)
    # Create a new table for BTCUSDT
    cry_db_create_symbol_table(db, "BTCUSDT")
    # Get the required dya to measure BTC from
    previous_day = cry_measure_date_string_DAY(cry_get_time_now())
    # Use klines to retrieve symbol data in 15m intervals
    BTC_data = cry_retrieve_symbol_data(client, "BTCUSDT", previous_day)
    # Trip Data accordingly
    BTC_data = cry_analysis_trim_data_DAY(BTC_data)
    # Insert Into Database
    cry_db_update_database(db, "BTCUSDT", BTC_data)
    # Design algorithm on 2% increase/decrease to suggest trades
    verdicts = cry_analysis_BTC_watcher_verdict(db)
    # Eventually automatically trade
    # - do something with returned object verdicts
    





def cry_analysis_command_pattern_watcher(client):
    print("")
    print("U N F I N I S H E D")
    print("")

def cry_analysis(client):

    print("")
    print("###########################")
    print("Welcome to Cry Project Auto Trader")
    print("###########################")

    cry_analysis_command_help()

    loop_status = CONTINUE_LOOP
    while loop_status:
        #action = input("\nEnter Command: ")
        action = 'b'

        if action == 'b':
            cry_analysis_command_BTC_watcher(client)
        elif action == 'p':
            cry_analysis_command_pattern_watcher(client)
        elif action == 'e':
            loop_status = END_LOOP
        else:
            cry_analysis_command_help()

    print("")
    print("###########################")
    print("Ending Cry Project Auto Trader")
    print("###########################")
        

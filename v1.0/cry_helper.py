# HELPER FILE FOR PYTHON BINANCE API

# Helper library for database operations
from cry_db_helper import cry_db_update_database, cry_db_create_database
import cry_db_config

# Time object libraries
from time import sleep
from datetime import datetime

# Function to get the current time
def cry_get_time_now():
    return datetime.now()

# Function to sleep or stop program
def cry_halt_program(t):
    t = int(t)
    while t >= 0:

        if t >= 5 and t % 5 == 0: print(f"...{t}")
        elif t < 5: print(f"...{t}")
        t -= 1
        sleep(1)

# Converts integer year month and day into a string for python-binance klines function
def cry_date_to_string(year, month, day):
    # Insert day
    output = f"{day} "
    # Insert month
    if month == 1: output += "Jan"
    elif month == 2: output += "Feb"
    elif month == 3: output += "Mar"
    elif month == 4: output += "Apr"
    elif month == 5: output += "may"
    elif month == 6: output += "Jun"
    elif month == 7: output += "Jul"
    elif month == 8: output += "Aug"
    elif month == 9: output += "Sep"
    elif month == 10: output += "Oct"
    elif month == 11: output += "Nov"
    elif month == 12: output += "Dec"
    # Insert year
    output += f",{year}"
    # Return final output string
    return output

# Converts the given time into a previous week date and given in string form
# we EDIT this depending on the size of historical data we wish to analyse
def cry_measure_date_string_WEEK(now):
    # Extract Year, month and day
    year = now.year
    month = now.month
    day = now.day
    # Calculate Previous week's measure date
    if day > 7: 
        day -= 7 
    elif month > 1:
        # assume 30 days for simplicity, won;t matter heavily, only on size of data collected for analysis
        month -= 1
        day = 30 + (day - 7) # gives us the new date object
    else:
        year -= 1
        month = 12
        day = 30 + (day - 7) 

    return cry_date_to_string(year, month, day)

# Finds previous day given a current time now object
def cry_measure_date_string_DAY(now):
    # Extract Year, month and day
    year = now.year
    month = now.month
    day = now.day
    # Calculate previous day string
    if day > 1:
        day -= 1
    elif month > 1:
        month -= 1
        day = 30
    else:
        year -= 1
        month = 12
        day = 30

    return cry_date_to_string(year, month, day)

# Returns Price of a Symbol given and its currency
def cry_symbol_price(client, symbol, currency):

    search_symbol = f"{symbol}{currency}"
    return float(client.get_symbol_ticker(symbol=search_symbol)['price'])

# Uses History Klines to retrieve data
def cry_retrieve_symbol_data(client, symbol, measure_day_string):

    print("")
    print("---------------------------")
    print(f"RETRIEVING and UPDATING data history: {symbol}")
    print("---------------------------")

    # Collect bulk open, high, low, close and etc data from binance
    klines = client.get_historical_klines(symbol, '15m', measure_day_string)
    # Design return object
    data = []
    # Loop through code and add closing price to array
    for k in klines: data.append(k[4]) # closing price of each interval
    # Return data object
    print("...success")
    return data

# Returns dictionary Object with {symbol, price}
def cry_client_wallet_prices(client, currency):
    print("")
    print("---------------------------")
    print("GETTING Wallet Asset Prices")
    print("---------------------------")
    balances = client.get_account()['balances']
    client_wallet_prices = {}
    for b in balances:
        asset = b['asset']
        amount = float(b['free'])
        if asset != currency and amount > 0: # checks for currency|currency scenario
            price = cry_symbol_price(client, asset, currency)
            print(f"Asset: {asset}{currency}, Amount: {amount}, Price: {price}")
            client_wallet_prices[f"{asset}{currency}"] = price
    
    return client_wallet_prices

def cry_client_wallet_assets(client, currency):

    print("")
    print("---------------------------")
    print("GETTING Wallet Assets")
    print("---------------------------")
    
    try:
        balances = client.get_account()['balances']
    except Exception as e:
        print(f"Error: Could not extract client balance information: {e}")
        exit(1)

    client_wallet_assets = []
    for b in balances:
        asset = b['asset']
        amount = float(b['free'])
        if asset != currency and amount > 0: # checks for currency|currency scenario
            print(f"Asset: {asset}{currency}")
            client_wallet_assets.append(f"{asset}{currency}")
    
    return client_wallet_assets

# Bulk implementation of database retrieval of cryptocurrency historical data
def cry_refresh_data(client):

    # Get Client Wallet Crypto Prices
    wallet_assets = cry_client_wallet_assets(client, "USDT")

    # Set up database
    db = cry_db_create_database(cry_db_config.database_path, wallet_assets)
    if not db: 
        print("Error: Could not create a database...exiting program")
        exit(1)

    # Find measure day
    measure_day_string = cry_measure_date_string_WEEK(cry_get_time_now())

    # Loop through assets and retrieve historical data on each
    for w in wallet_assets: 
        # Retrieve data
        price_list = cry_retrieve_symbol_data(client, w, measure_day_string)
        # Update database symbol table
        cry_db_update_database(db, w, price_list)

# Prints the starting output for the script
def cry_start_output():
    print("")
    print("###########################")
    print("STARTING Cry cript")
    print("###########################")

# Prints the starting output for the script
def cry_finish_output():
    print("")
    print("###########################")
    print("FINISHING Cry Script")
    print("###########################")
    print("")


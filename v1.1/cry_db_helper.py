# SQLite3 Library
import sqlite3
# Datetime librari
from datetime import datetime
# Cry Helper Library
from cry_helper import cry_halt_program
# Cry Config Helper Library
from cry_config_helper import cry_config_get_database_filepath
# JSON Library Methods
import json

# Connect to database
def cry_db_connect(path):

    try:
        return sqlite3.connect(path)
    except Exception as e:
        print(f"Error: Could not open a connection")
        return None

# Insert Single Element into the Database
def cry_db_insert_element(db, table_name, element):
    q = f'''INSERT INTO {table_name} (price) VALUES ({element})'''
    # Try and insert into database
    try:
        cur = db.cursor()
        cur.execute(q)
        cur.close()
    except Exception as e:
        print(f"Error: Could not update {symbol} | {p} into database: {e}")

    db.commit()

# Insert List of Elements into the Database
def cry_db_insert_elements(db, table_name, elements):
    # Cycle through List and insert single elements
    for e in elements: cry_db_insert_element(db, table_name, float(e))

# Function to remove an entire table and it's elements from the database
def cry_db_delete_table(db, table_name):
    # Query to remove table
    q = f'''drop table {table_name}'''
    # Attempt to delete the table
    try:
        cur = db.cursor()
        cur.execute(q)
        cur.close()
        db.commit()
        return 0
    except Exception as e: # catch error and determine what went wrong
        return 1

# Create Table in SQL
def cry_db_create_table(db, table_name):

    # We first drop any pre existing table
    drop_status = cry_db_delete_table(db, table_name)
    # drop status indicates whether this worked, we have no need to catch this for now

    # Query Form to generate new table
    q = f'''CREATE TABLE {table_name} (price float NOT NULL)'''
    # Print to command line
    print(q)
    # Attempt to create the table
    try:
        cur = db.cursor()
        cur.execute(q)
        cur.close()
        db.commit()
        return None
    except Exception as e:
        print(f"Error Creating Symbol Table {table_name}: {e}")

    print("...removing table and retrying")
    q_remove = f'''DROP TABLE {table_name}'''

    # Attempt to remove table and add it back
    try:
        cur = db.cursor()
        cur.execute(q_remove)
        cur.execute(q)
        db.commit()
    except Exception as e:
        print(f"Error: Could not remove and create the symbol table {table_name}: {e}")
        return None

    return

# Uses History Klines to retrieve data
def cry_db_retrieve_table_data(client, symbol, measure_day_string):

    print("")
    print("---------------------------")
    print(f"RETRIEVING and UPDATING data history: {symbol}")
    print("---------------------------")

    # Collect bulk open, high, low, close and etc data from binance
    try:
        klines = client.get_historical_klines(symbol, '15m', measure_day_string)
    except Exception as e:
        print("Error: Connection timed out...retrying\n")
        print("...HALTING request")
        cry_halt_program(5)
        return cry_db_retrieve_table_data(client, symbol, measure_day_string) # recursive function to ensure good data return
    # Design return object
    data = []
    # Loop through code and add closing price to array
    for k in klines: data.append(k[4]) # closing price of each interval
    # Return data object
    print("...success")
    return data

# Initialise Database Tables
def cry_db_create_database(wallet_assets):

    print("")
    print("---------------------------")
    print("RESETTING Database(s)")
    print("---------------------------")

    # Get database connection
    db = cry_db_connect(cry_config_get_database_filepath())
    if not db: return None

    # Generate SQL tables for all assets
    for w in wallet_assets: cry_db_create_table(db, w)

    # Return connection to the database
    return db

# Uses The List of wallet assets and a measuring day to insert all new data into the database
def cry_db_collect_data(client, wallet_assets, db, measure_day):

    print("")
    print("---------------------------")
    print("COLLECTING Data")
    print("---------------------------")

    # Loop through wallet assets and extract dataset of each
    for w in wallet_assets:
        # Retrieve table data
        data = cry_db_retrieve_table_data(client, w, measure_day)
        # Update database table with new dataset
        cry_db_insert_elements(db, w, data)

    return

# Collects entire dta set from given table name
def cry_db_collect_table_data(db, table_name):

    q = f'''select * from {table_name}'''
    # Attempt to execute database query
    try:
        cur = db.cursor()
        cur.execute(q)
        data = cur.fetchall()
    except Exception as e:
        print(f"Error: Could not collect table data from {table_name}: {e}")
        return []

    # Now trim dataset to fix it up
    trimmed_data = []
    for d in data:
        trimmed_data.append(d[0])
    return trimmed_data


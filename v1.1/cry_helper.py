# Collective Functions used between the three scripts

# Binance API Python Library
from binance.client import Client
# Time Object Libraries
from time import sleep
# Datetime Libraries
from datetime import datetime
# JSON Library
import json
# Cry Config Helper Library
from cry_config_helper import cry_config_get_api_key, cry_config_get_api_security # API Section
from cry_config_helper import cry_config_get_halt_time
from cry_config_helper import cry_config_get_database_filepath, cry_config_get_collection_date, cry_config_get_currency # DATABASE Section
from cry_config_helper import cry_config_get_quiver_filepath # ANALYSIS Section

# Extract Client Information from Binance API
def cry_get_client():
    return Client(cry_config_get_api_key(), cry_config_get_api_security())

# Function to get the current time
def cry_get_time_now():
    return datetime.now()

# Function to sleep or stop program
def cry_halt_program(t = cry_config_get_halt_time()):    
    # Type cast to integer
    t = int(t)
    # While time is valid
    while t >= 0:
        # If there is more than 5 seconds, count in 10 sec intervals
        if t > 5 and t % 10 == 0: print(f"...{t} seconds")
        # else, print all numbers
        elif t <= 5: print(f"...{t} seconds")
        t -= 1
        # sleep for 1 second inbetween timers
        sleep(1)

# Converts integer year month and day into a string for python-binance klines function
def cry_date_to_string(year, month, day):
    # Insert day
    output = f"{day} "
    # Insert month
    if month == 1: output += "Jan"
    elif month == 2: output += "Feb"
    elif month == 3: output += "Mar"
    elif month == 4: output += "Apr"
    elif month == 5: output += "may"
    elif month == 6: output += "Jun"
    elif month == 7: output += "Jul"
    elif month == 8: output += "Aug"
    elif month == 9: output += "Sep"
    elif month == 10: output += "Oct"
    elif month == 11: output += "Nov"
    elif month == 12: output += "Dec"
    # Insert year
    output += f",{year}"
    # Return final output string
    return output

# Function to convert given time now into year, month and day
def cry_time_variables(now):
    return [now.year, now.month, now.day]

# Converts the given time into a previous week date and given in string form
def cry_measure_date_string_WEEK(now):
    # Extract Year, month and day
    [year, month, day] = cry_time_variables(now)
    # Calculate Previous week's measure date
    if day > 7: 
        day -= 7 
    elif month > 1:
        # assume 30 days for simplicity, won't matter heavily, only on size of data collected for analysis
        month -= 1
        day = 30 + (day - 7) # gives us the new date object
    else:
        year -= 1
        month = 12
        day = 30 + (day - 7) 

    return cry_date_to_string(year, month, day)

# Finds previous day given a current time now object
def cry_measure_date_string_DAY(now):
    # Extract Year, month and day
    [year, month, day] = cry_time_variables(now)
    # Calculate previous day string
    if day > 1:
        day -= 1
    elif month > 1:
        month -= 1
        day = 30
    else:
        year -= 1
        month = 12
        day = 30

    return cry_date_to_string(year, month, day)

# Finds previous year date given a current time now object
def cry_measure_date_string_YEAR(now):
    # Extract Year, month and day
    [year, month, day] = cry_time_variables(now)
    # Calculate previous year string
    year -= 1;
    return cry_date_to_string(year, month, day)

# Function to get the appropriate measure date from config file
def cry_measure_date():
    if cry_config_get_collection_date().lower() == "day": return cry_measure_date_string_DAY(cry_get_time_now())
    elif cry_config_get_collection_date().lower() == "week": return cry_measure_date_string_WEEK(cry_get_time_now())
    elif cry_config_get_collection_date().lower() == "year": return cry_measure_date_string_YEAR(cry_get_time_now())
    return "ERROR: Could not extract collection back date"

# Uses History Klines to retrieve data
def cry_retrieve_symbol_data(client, symbol, measure_day_string):

    print("")
    print("---------------------------")
    print(f"RETRIEVING and UPDATING data history: {symbol}")
    print("---------------------------")

    # Collect bulk open, high, low, close and etc data from binance
    klines = client.get_historical_klines(symbol, '15m', measure_day_string)
    # Design return object
    data = []
    # Loop through code and add closing price to array
    for k in klines: data.append(k[4]) # closing price of each interval
    # Return data object
    print("...success")
    return data

# Returns dictionary Object with {symbol, price}
def cry_client_wallet_prices(client, currency):
    print("")
    print("---------------------------")
    print("GETTING Wallet Asset Prices")
    print("---------------------------")
    balances = client.get_account()['balances']
    client_wallet_prices = {}
    for b in balances:
        asset = b['asset']
        amount = float(b['free'])
        if asset != currency and amount > 0: # checks for currency|currency scenario
            price = cry_symbol_price(client, asset, currency)
            print(f"Asset: {asset}{currency}, Amount: {amount}, Price: {price}")
            client_wallet_prices[f"{asset}{currency}"] = price
    
    return client_wallet_prices

def cry_client_wallet_assets(client):

    print("")
    print("---------------------------")
    print("GETTING Wallet Assets")
    print("---------------------------")
    
    try:
        balances = client.get_account()['balances']
    except Exception as e:
        print(f"Error: Could not extract client balance information: {e}")
        exit(1)

    currency = cry_config_get_currency()

    client_wallet_assets = []
    quiver_assets = {}
    for b in balances:
        asset = b['asset']
        amount = float(b['free'])
        if asset != currency and amount > 0: # checks for currency|currency scenario
            print(f"Asset: {asset}{currency}: {amount}")
            client_wallet_assets.append(f"{asset}{currency}")
            quiver_assets[f'{asset}{currency}'] = amount
    
    return [client_wallet_assets, quiver_assets]

# Bulk implementation of database retrieval of cryptocurrency historical data
def cry_refresh_data(client):

    # Get Client Wallet Crypto Prices
    wallet_assets = cry_client_wallet_assets(client, "USDT")

    # Set up database
    db = cry_db_create_database(cry_config_get_database_filepath(), wallet_assets)
    if not db: 
        print("Error: Could not create a database...exiting program")
        exit(1)

    # Find measure day
    measure_day_string = cry_measure_date_string_WEEK(cry_get_time_now())

    # Loop through assets and retrieve historical data on each
    for w in wallet_assets: 
        # Retrieve data
        price_list = cry_retrieve_symbol_data(client, w, measure_day_string)
        # Update database symbol table
        cry_db_update_database(db, w, price_list)

# Function to retrieve quiver information from JSON File
def cry_read_quiver():

    # Output for function
    print("")
    print("---------------------------")
    print(f"RETRIEVING Quiver Data")
    print("---------------------------")
    # Attempt to open quiver json data into script
    with open(cry_config_get_quiver_filepath()) as json_file:
        try:
            json_object = json.load(json_file)
            json_file.close()
        except Exception as e:
            print(f"Error: Could not open quiver data into json object: {e}")
            return
    # Loop through json object and collect keys and key values
    final_object = {}
    wallet_assets = []
    for j in json_object:
        wallet_assets.append(j)
        final_object[j] = json_object[j]

    print("...success")
    return [final_object, wallet_assets]

# Function to update quiver with current data
def cry_update_quiver(quiver_assets):

    # Output for function
    print("")
    print("---------------------------")
    print(f"UPDATING Quiver Data")
    print("---------------------------")

    json_object = json.dumps(quiver_assets, indent=4)

    with open(cry_config_get_quiver_filepath(), "w") as json_file:
        try:
            json_file.write(json_object)
        except Exception as e:
            print(f"Error: Unable to Write Asset Details into Quiver System: {e}")
            return

    print("...success")
    return




# Cry Database Scripts that Maintain the SQL Database with Fresh Entries from 24 hr change

# Cry Helper Library
from cry_helper import cry_halt_program, cry_get_client, cry_client_wallet_assets, cry_update_quiver, cry_measure_date
# Cry Database Helper Library
from cry_db_helper import cry_db_create_database, cry_db_collect_data
# Cry Config Helper Library
from cry_config_helper import cry_config_get_refresh_time

def cry_db_start_output():
    print("")
    print("##########################################")
    print("Welcome to Project Cry: Database Collector")
    print("##########################################")

def cry_db_finish_output():
    print("")
    print("##########################################")
    print("Ending Project Cry: Database Collector")
    print("##########################################")

# Starting Output for Application
cry_db_start_output()

# Output To Start Collection
print("\n...COLLECTION starting in")
cry_halt_program();

# Get Client from Binance API
client = cry_get_client()

# Get Measure Day in appropriate string form
measure_day = cry_measure_date()

# Refresh Data Set
while (True):

    # Extract Wallet Assets from Client
    [wallet_assets, quiver_assets] = cry_client_wallet_assets(client)

    # Update JSON Quiver
    cry_update_quiver(quiver_assets)

    # Create database with wallet assets list as table names
    db = cry_db_create_database(wallet_assets)

    # Get New Data Change
    cry_db_collect_data(client, wallet_assets, db, measure_day)

    # Output To Start Recollection
    print("\n...RECOLLECTION starting in")
    cry_halt_program(cry_config_get_refresh_time()); # 15 mins (since we collect data on 15m intervals, long time for software to extract data)
    # approximately 30-40 seconds where database is incomplete with data set.  If algorithm detects no data, run 10 second halts


# Finishing Output from Database Collector Script
cry_db_finish_output()
# Project Cry Analysis Scripts to Analyse Data and design trades

# Cry Helper Library
from cry_helper import cry_halt_program, cry_read_quiver
# Cry Analysis Helper Functions
from cry_analysis_helper import cry_analysis_datasets

# Starting Output for Cry Analysis
def cry_analysis_start_output():
    print("")
    print("##########################################")
    print("Welcome to Project Cry: Analysis Tool")
    print("##########################################")

# Finishing Output for Cry Analysis
def cry_analysis_finish_output():
    print("")
    print("##########################################")
    print("Ending Project Cry: Analysis")
    print("##########################################")

# Print starting output
cry_analysis_start_output()

# Output To Start Analysis
print("\n...ANALYSIS starting in")
cry_halt_program();

# Loop through endlessely and analyse data
#while (True):
    # Retrieve Quiver Information
    #[asset_ownership, wallet_assets] = cry_read_quiver()

    # Analyse sets of data and deposit results into arrow systems
    #cry_analysis_datasets(wallet_assets)

    # Output To restart analysis
    #print("\n...RECOLLECTION starting in")
    #cry_halt_program(30);

# Let's play with some data science methods with neural network machine learning for stock prediction




# Analyse

# Print finish output
cry_analysis_finish_output()
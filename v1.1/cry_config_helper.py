# Helper Functions to assist with extracting data in the configuration files

# Config Parser import
import configparser

# Setup Config file path
config_setup_path = './config/setup.cfg'

# API Config file path
config_api_path = './config/api.cfg'

# Parser represents configparser object
config_setup = configparser.ConfigParser()
config_api = configparser.ConfigParser()

# Config Setup file
config_setup.read(config_setup_path)

# API Config File
config_api.read(config_api_path)

############################
# CONFIG API FUNCTIONS
############################

def cry_config_get_api_key(): return config_api['APIKEYS']['api_key']

def cry_config_get_api_security(): return config_api['APIKEYS']['api_security']

############################
# CONFIG GENERAL FUNCTIONS
############################

def cry_config_get_halt_time(): return config_setup['GENERAL']['halt_time']

############################
# CONFIG DATABASE FUNCTIONS
############################

def cry_config_get_database_filepath(): return config_setup['DATABASE']['database_filepath']

def cry_config_get_currency(): return config_setup['DATABASE']['currency']

def cry_config_get_collection_date(): return config_setup['DATABASE']['collection_back_date']

def cry_config_get_refresh_time(): return config_setup['DATABASE']['refresh_time']

############################
# CONFIG ANALYSIS FUNCTIONS
############################

def cry_config_get_quiver_filepath(): return config_setup['ANALYSIS']['quiver_path']

############################
# CONFIG TRADING FUNCTIONS
############################

def cry_config_get_arrow_filepath(): return config_setup['TRADING']['arrow_path']

